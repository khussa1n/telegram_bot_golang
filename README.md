# KhussainGolangHolidayBot

Telegram bot to determine International holidays and memorable dates and save your holidays

## Getting started

```
docker-compose up --build bot

```

If you want to run your binary use these commands

```
go mod download
make build
make docker-compose-run

```

***
